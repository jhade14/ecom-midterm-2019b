<?php 
	include('connection.php');

	$sql = "SELECT * FROM user";

		if ($_GET['sort'] == 'asc')
		{
		    $sql .= " ORDER BY lname ASC";
		}
		elseif ($_GET['sort'] == 'desc')
		{
		    $sql .= " ORDER BY lname DESC";
		}
		elseif ($_GET['sort'] == 'lname')
		{
		    $sql .= " ORDER BY id DESC";
		}
		elseif ($_GET['sort'] == 'contact')
		{
		    $sql .= " ORDER BY contact";
		}
		elseif ($_GET['sort'] == 'email')
		{
		    $sql .= " ORDER BY email";
		}

		$result = mysqli_query($conn, $sql);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<br>
	<br>
	<div class="container">
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<dir class="" style="float: right;">
				<input type="button" name="" value="+ ADD" onclick="location.href='add.html';">
			</dir>
			<div class="dropdown">
			  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By:
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				  	<li><a href="index.php?sort=lname">Default</a></li>
				    <li><a href="index.php?sort=asc">Last name(ascending)</a></li>
				    <li><a href="index.php?sort=desc">Last name(descending)</a></li>
				    <li><a href="index.php?sort=contact">Contact</a></li>
				    <li><a href="index.php?sort=email">Email</a></li>
			  	</ul>
			</div>
			<br/><br/>
			<table class="table table-bordered table-striped" width='80%' border=0>
				<tr bgcolor="#CCCCCC">
					<td>Name</td>
					<td> </td>
					<td>Contact No.</td>
					<td>Email</td>
					<td></td>
				</tr>
			<?php 
				while ($res = mysqli_fetch_array($result)) {
					echo "<tr>";
					echo "<td>".$res['lname']."</td>";
					echo "<td>".$res['fname']."</td>";
					echo "<td>".$res['contact']."</td>";
					echo "<td>".$res['email']."</td>";
					echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";
				}
			 ?>
			</table>
		</form>
	</div>
</body>
</html>